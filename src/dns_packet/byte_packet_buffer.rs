use log::debug;
use std::io::{Error, ErrorKind, Result};

pub struct BytePacketBuffer {
    pub buf: [u8; 512],
    pub pos: usize,
}

impl Default for BytePacketBuffer {
    fn default() -> Self {
        Self {
            buf: [0; 512],
            pos: 0,
        }
    }
}

impl BytePacketBuffer {
    pub fn step(&mut self, steps: usize) -> Result<()> {
        self.pos += steps;
        Ok(())
    }

    pub fn seek(&mut self, pos: usize) -> Result<()> {
        self.pos = pos;
        Ok(())
    }

    pub fn get(&mut self, pos: usize) -> Result<u8> {
        if pos >= self.buf.len() {
            return Err(Error::new(ErrorKind::InvalidInput, "End of buffer"));
        }
        Ok(self.buf[pos])
    }

    pub fn get_range(&mut self, start: usize, len: usize) -> Result<&[u8]> {
        if start + len >= self.buf.len() {
            return Err(Error::new(ErrorKind::InvalidInput, "End of buffer"));
        }
        Ok(&self.buf[start..start + len as usize])
    }

    pub fn raw_buffer(&self) -> &[u8] {
        &self.buf[0..self.pos]
    }

    fn read(&mut self) -> Result<u8> {
        if self.pos >= self.buf.len() {
            return Err(Error::new(ErrorKind::InvalidInput, "End of buffer"));
        }

        self.pos += 1;
        Ok(self.buf[self.pos - 1])
    }

    pub fn read_u8(&mut self) -> Result<u8> {
        Ok(self.read()?)
    }

    pub fn read_u16(&mut self) -> Result<u16> {
        let mut ret: u16 = 0;
        let shifts: &[u8] = &[8, 0];
        for shift in shifts {
            ret = ret | (self.read()? as u16) << shift;
        }
        Ok(ret)
    }

    pub fn read_u32(&mut self) -> Result<u32> {
        let mut ret: u32 = 0;
        let shifts: &[u8] = &[24, 16, 8, 0];
        for shift in shifts {
            ret = ret | (self.read()? as u32) << shift;
        }
        Ok(ret)
    }

    // This is apparently the tricky part.
    pub fn read_qname(&mut self) -> Result<String> {
        let mut local_pos = self.pos;
        let mut jumped = false;
        let mut domain_parts: Vec<String> = vec![];

        loop {
            let len = self.get(local_pos)?;
            debug!("len: {:?}", len);

            // if len has the two most significant bits set, it represents a jump to some other
            // offset in the packet
            if (len & 0xC0) == 0xC0 {
                debug!("Jumping");
                if !jumped {
                    debug!("Seeking: {:?}", local_pos + 2);
                    self.seek(local_pos + 2)?;
                }

                debug!("get({:?})", local_pos + 1);
                let b2 = self.get(local_pos + 1)? as u16;
                debug!("-> {:?}", b2);
                let offset = (((len as u16) ^ 0xC0) << 8) | b2;
                local_pos = offset as usize;
                debug!("offset: {:?}", offset);

                jumped = true;
                continue;
            }

            // Base scenario
            local_pos += 1;

            if len == 0 {
                break;
            }

            let str_buffer = self.get_range(local_pos, len as usize)?;
            domain_parts.push(String::from_utf8_lossy(str_buffer).to_lowercase());

            local_pos += len as usize;
        }

        if !jumped {
            self.seek(local_pos)?;
        }

        Ok(domain_parts.join("."))
    }

    fn write(&mut self, val: u8) -> Result<()> {
        if self.pos >= self.buf.len() {
            return Err(Error::new(ErrorKind::InvalidInput, "End of buffer"));
        }

        self.buf[self.pos] = val;
        self.pos += 1;
        Ok(())
    }

    pub fn write_u8(&mut self, val: u8) -> Result<()> {
        self.write(val)
    }

    pub fn write_u16(&mut self, val: u16) -> Result<()> {
        let shifts: &[u8] = &[8, 0];
        for shift in shifts.into_iter() {
            self.write(((val >> shift) & 0xFF) as u8)?;
        }
        Ok(())
    }

    #[allow(exceeding_bitshifts)]
    pub fn write_u32(&mut self, val: u32) -> Result<()> {
        let shifts: &[u8] = &[24, 16, 8, 0];
        for shift in shifts.into_iter() {
            self.write(((val >> shift) & 0xFF) as u8)?;
        }
        Ok(())
    }

    pub fn write_qname(&mut self, qname: &str) -> Result<()> {
        let domain_parts: Vec<&str> = qname.split(".").collect();

        for label in domain_parts {
            let len = label.len();
            if len > 0x34 {
                return Err(Error::new(
                    ErrorKind::InvalidInput,
                    "Single label exceeds 63 characters of length",
                ));
            }

            self.write_u8(len as u8)?;
            for byte in label.as_bytes() {
                self.write_u8(*byte)?;
            }
        }

        self.write_u8(0)?;

        Ok(())
    }

    fn set(&mut self, pos: usize, val: u8) -> Result<()> {
        self.buf[pos] = val;
        Ok(())
    }

    pub fn set_u8(&mut self, pos: usize, val: u8) -> Result<()> {
        self.set(pos, val)
    }

    pub fn set_u16(&mut self, pos: usize, val: u16) -> Result<()> {
        let shifts: &[u8] = &[8, 0];
        for (pos_modifier, shift) in shifts.iter().enumerate() {
            self.set(pos + pos_modifier, ((val >> shift) & 0xFF) as u8)?
        }
        Ok(())
    }
}
