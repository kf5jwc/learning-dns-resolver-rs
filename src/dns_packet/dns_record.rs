use super::BytePacketBuffer;
use super::QueryType;
use std::io::Result;
use std::net::{Ipv4Addr, Ipv6Addr};

#[derive(Debug, Clone, PartialEq, Eq, Hash, PartialOrd, Ord)]
#[allow(dead_code)]
pub enum DnsRecord {
    UNKNOWN {
        domain: String,
        qtype: u16,
        data_len: u16,
        ttl: u32,
    }, // 0
    A {
        domain: String,
        addr: Ipv4Addr,
        ttl: u32,
    }, // 1
    NS {
        domain: String,
        host: String,
        ttl: u32,
    }, // 2
    CNAME {
        domain: String,
        host: String,
        ttl: u32,
    }, // 5
    MX {
        domain: String,
        priority: u16,
        host: String,
        ttl: u32,
    }, // 15
    AAAA {
        domain: String,
        addr: Ipv6Addr,
        ttl: u32,
    }, // 28
}

impl DnsRecord {
    pub fn read(buffer: &mut BytePacketBuffer) -> Result<Self> {
        let domain = buffer.read_qname()?;
        let qtype: QueryType = buffer.read_u16()?.into();
        let _ = buffer.read_u16()?; // class
        let ttl = buffer.read_u32()?;
        let data_len = buffer.read_u16()?;

        match qtype {
            QueryType::A => {
                let raw_addr = buffer.read_u32().expect("Next raw address group");
                let addr_groups: Vec<u8> = [24u8, 16, 8, 0]
                    .iter()
                    .map(|shift| ((raw_addr >> shift) & 0xFF) as u8)
                    .collect();
                let addr = Ipv4Addr::new(
                    addr_groups[0],
                    addr_groups[1],
                    addr_groups[2],
                    addr_groups[3],
                );

                Ok(DnsRecord::A {
                    domain: domain,
                    addr: addr,
                    ttl: ttl,
                })
            }
            QueryType::AAAA => {
                let raw_addrs: Vec<u32> = (0..8)
                    .map(|_| buffer.read_u32().expect("Next raw address group"))
                    .collect();
                let addr_groups: Vec<u16> = {
                    let shifts = vec![16u8, 0].into_iter().cycle();
                    raw_addrs
                        .iter()
                        .zip(shifts)
                        .map(|(raw_addr, shift)| ((raw_addr >> shift) & 0xFFFF) as u16)
                        .collect()
                };
                let addr = Ipv6Addr::new(
                    addr_groups[0],
                    addr_groups[1],
                    addr_groups[2],
                    addr_groups[3],
                    addr_groups[4],
                    addr_groups[5],
                    addr_groups[6],
                    addr_groups[7],
                );

                Ok(DnsRecord::AAAA {
                    domain: domain,
                    addr: addr,
                    ttl: ttl,
                })
            }

            // NS and CNAME both have the same structure.
            QueryType::NS => Ok(DnsRecord::NS {
                domain: domain,
                host: buffer.read_qname()?,
                ttl: ttl,
            }),

            QueryType::CNAME => Ok(DnsRecord::CNAME {
                domain: domain,
                host: buffer.read_qname()?,
                ttl: ttl,
            }),

            // MX is almost like the previous two, but with one extra field for priority.
            QueryType::MX => Ok(DnsRecord::MX {
                domain: domain,
                priority: buffer.read_u16()?,
                host: buffer.read_qname()?,
                ttl: ttl,
            }),

            QueryType::UNKNOWN(qtype_num) => {
                buffer.step(data_len as usize)?;

                Ok(DnsRecord::UNKNOWN {
                    domain: domain,
                    qtype: qtype_num,
                    data_len: data_len,
                    ttl: ttl,
                })
            }
        }
    }

    pub fn write(&self, buffer: &mut BytePacketBuffer) -> Result<usize> {
        let start_pos = buffer.pos;

        match *self {
            Self::A {
                ref domain,
                ref addr,
                ttl,
            } => {
                buffer.write_qname(domain)?;
                buffer.write_u16(QueryType::A.into())?;
                buffer.write_u16(1)?;
                buffer.write_u32(ttl)?;
                buffer.write_u16(4)?;

                let octets = addr.octets();
                buffer.write_u8(octets[0])?;
                buffer.write_u8(octets[1])?;
                buffer.write_u8(octets[2])?;
                buffer.write_u8(octets[3])?;
            }

            Self::NS {
                ref domain,
                ref host,
                ttl,
            } => {
                buffer.write_qname(domain)?;
                buffer.write_u16(QueryType::NS.into())?;
                buffer.write_u16(1)?;
                buffer.write_u32(ttl)?;

                let pre_pos = buffer.pos;
                buffer.write_u16(0)?;
                buffer.write_qname(host)?;

                let size = buffer.pos - (pre_pos + 2);
                buffer.set_u16(pre_pos, size as u16)?;
            }

            Self::CNAME {
                ref domain,
                ref host,
                ttl,
            } => {
                buffer.write_qname(domain)?;
                buffer.write_u16(QueryType::CNAME.into())?;
                buffer.write_u16(1)?;
                buffer.write_u32(ttl)?;

                let pre_pos = buffer.pos;
                buffer.write_u16(0)?;
                buffer.write_qname(host)?;

                let size = buffer.pos - (pre_pos + 2);
                buffer.set_u16(pre_pos, size as u16)?;
            }

            Self::MX {
                ref domain,
                priority,
                ref host,
                ttl,
            } => {
                buffer.write_qname(domain)?;
                buffer.write_u16(QueryType::MX.into())?;
                buffer.write_u16(1)?;
                buffer.write_u32(ttl)?;

                let pre_pos = buffer.pos;
                buffer.write_u16(0)?;

                buffer.write_u16(priority)?;
                buffer.write_qname(host)?;

                let size = buffer.pos - (pre_pos + 2);
                buffer.set_u16(pre_pos, size as u16)?;
            }

            Self::AAAA {
                ref domain,
                ref addr,
                ttl,
            } => {
                buffer.write_qname(domain)?;
                buffer.write_u16(QueryType::AAAA.into())?;
                buffer.write_u16(1)?;
                buffer.write_u32(ttl)?;
                buffer.write_u16(16)?;

                for octet in &addr.segments() {
                    buffer.write_u16(*octet)?;
                }
            }

            Self::UNKNOWN { .. } => {
                println!("Skipping record: {:?}", self);
            }
        }

        Ok(buffer.pos - start_pos)
    }
}
