use std::str::FromStr;

#[derive(PartialEq, Eq, Debug, Clone, Hash, Copy)]
pub enum QueryType {
    UNKNOWN(u16),
    A,     // 1
    NS,    // 2
    AAAA,  // 28
    CNAME, // 5
    MX,    // 15
           //SRV, // ?
           //CERT, // ?
           //TXT, // ?
}

impl Into<u16> for QueryType {
    fn into(self) -> u16 {
        match self {
            Self::UNKNOWN(x) => x,
            Self::A => 1,
            Self::NS => 2,
            Self::CNAME => 5,
            Self::MX => 15,
            Self::AAAA => 28,
        }
    }
}

impl Into<u32> for QueryType {
    fn into(self) -> u32 {
        Into::<u16>::into(self) as u32
    }
}

impl From<u32> for QueryType {
    fn from(num: u32) -> Self {
        Into::<QueryType>::into(num as u16)
    }
}

impl From<u16> for QueryType {
    fn from(num: u16) -> Self {
        match num {
            01 => Self::A,
            02 => Self::NS,
            05 => Self::CNAME,
            15 => Self::MX,
            28 => Self::AAAA,
            _ => Self::UNKNOWN(num),
        }
    }
}

impl FromStr for QueryType {
    type Err = &'static str;

    fn from_str(string: &str) -> Result<Self, Self::Err> {
        match string {
            "A" => Ok(Self::A),
            "NS" => Ok(Self::NS),
            "AAAA" => Ok(Self::AAAA),
            "CNAME" => Ok(Self::CNAME),
            "MX" => Ok(Self::MX),
            _ => Err("Unsupported record type!"),
        }
    }
}
