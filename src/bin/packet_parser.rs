extern crate dns_server;
extern crate env_logger;
extern crate log;
extern crate structopt;

use dns_server::{BytePacketBuffer, DnsPacket};
use std::fs::File;
use std::io::Read;
use std::path::PathBuf;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
#[structopt()]
struct Args {
    #[structopt(parse(from_os_str), default_value = "packets/response_packet")]
    packet_file: PathBuf,
}

fn main() {
    env_logger::init();
    let args = Args::from_args();

    let mut f = File::open(args.packet_file).unwrap();
    let mut res_buffer = BytePacketBuffer::default();
    f.read(&mut res_buffer.buf).unwrap();

    let res_packet: DnsPacket = res_buffer.into();
    eprintln!("{:?}", res_packet.header);

    for q in res_packet.questions {
        eprintln!("{:?}", q);
    }
    for rec in res_packet.answers {
        eprintln!("{:?}", rec);
    }
    for rec in res_packet.authorities {
        eprintln!("{:?}", rec);
    }
    for rec in res_packet.resources {
        eprintln!("{:?}", rec);
    }
}
