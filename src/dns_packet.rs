mod byte_packet_buffer;
mod dns_header;
mod dns_question;
mod dns_record;
mod query_type;

pub use byte_packet_buffer::BytePacketBuffer;
pub use dns_header::{DnsHeader, ResultCode};
pub use dns_question::DnsQuestion;
pub use dns_record::DnsRecord;
use log::debug;
pub use query_type::QueryType;

#[derive(Default, Clone, Debug)]
pub struct DnsPacket {
    pub header: DnsHeader,
    pub questions: Vec<DnsQuestion>,
    pub answers: Vec<DnsRecord>,
    pub authorities: Vec<DnsRecord>,
    pub resources: Vec<DnsRecord>,
}

impl Into<BytePacketBuffer> for DnsPacket {
    fn into(mut self) -> BytePacketBuffer {
        debug!("DnsPacket -> BytePacketBuffer");
        self.header.questions = self.questions.len() as u16;
        debug!("Questions: {}", self.header.questions);
        self.header.answers = self.answers.len() as u16;
        debug!("Answers: {}", self.header.questions);
        self.header.authoritative_entries = self.authorities.len() as u16;
        debug!("Authoritative entries: {}", self.header.questions);
        self.header.resource_entries = self.resources.len() as u16;
        debug!("Resource entries: {}", self.header.questions);

        let mut buffer = BytePacketBuffer::default();
        self.header.write(&mut buffer).expect("Writing to header");

        for question in &self.questions {
            question
                .write(&mut buffer)
                .expect("Writing questions to buffer");
        }
        for rec in &self.answers {
            rec.write(&mut buffer)
                .expect("Writing answer records to buffer");
        }
        for rec in &self.authorities {
            rec.write(&mut buffer)
                .expect("Writing authority records to buffer");
        }
        for rec in &self.resources {
            rec.write(&mut buffer)
                .expect("Writing resource records to buffer");
        }
        return buffer;
    }
}

impl From<BytePacketBuffer> for DnsPacket {
    fn from(mut buffer: BytePacketBuffer) -> Self {
        let mut result = DnsPacket::default();
        result
            .header
            .read(&mut buffer)
            .expect("Error reading header from packet buffer!");

        for _ in 0..result.header.questions {
            let mut question = DnsQuestion {
                name: "".to_string(),
                qtype: QueryType::UNKNOWN(0),
            };
            question
                .read(&mut buffer)
                .expect("Error reading question from packet buffer!");
            result.questions.push(question);
        }

        for _ in 0..result.header.answers {
            let rec = DnsRecord::read(&mut buffer).expect("Error reading answers!");
            result.answers.push(rec);
        }

        for _ in 0..result.header.authoritative_entries {
            let rec = DnsRecord::read(&mut buffer).expect("Error reading authoritative entries!");
            result.authorities.push(rec);
        }

        for _ in 0..result.header.resource_entries {
            let rec = DnsRecord::read(&mut buffer).expect("Error reading resourc_entries!");
            result.resources.push(rec);
        }

        result
    }
}
