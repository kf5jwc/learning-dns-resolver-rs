mod dns_packet;

pub use dns_packet::{BytePacketBuffer, DnsPacket, DnsQuestion, QueryType, ResultCode};
